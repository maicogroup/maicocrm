﻿using System;
using maicoCRM.Areas.Identity.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: HostingStartup(typeof(maicoCRM.Areas.Identity.IdentityHostingStartup))]
namespace maicoCRM.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<maicoCRMContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("maicoCRMContextConnection")));

                services.AddDefaultIdentity<maicoCRMUser>(options => options.SignIn.RequireConfirmedAccount = true)
                    .AddEntityFrameworkStores<maicoCRMContext>();
            });
        }
    }
}